<?php

namespace AppBundle\Entity;

use AppBundle\DBAL\Types\CardholderStatusType;
use AppBundle\Model\TimestampableModel;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cardholder.
 *
 * @ORM\Table(name="cardholders")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CardholderRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("number")
 */
class Cardholder extends TimestampableModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="middle_name", type="string", length=255, nullable=true)
     */
    private $middleName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=255)
     */
    private $gender;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^\+7\d{10}$/")
     *
     * @ORM\Column(name="phone", type="string", length=16)
     */
    private $phone;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="car", type="string", length=255)
     */
    private $car;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="car_number", type="string", length=10)
     */
    private $carNumber;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="car_year", type="integer")
     */
    private $carYear;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(name="job", type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @var string|null
     *
     * @ORM\Column(name="position", type="string", length=40, nullable=true)
     */
    private $position;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="6", max="6")
     *
     * @ORM\Column(name="number", type="string", length=6, unique=true)
     */
    private $number;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="until", type="datetime", nullable=true)
     */
    private $until;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="status", type="CardholderStatusType", nullable=false, options={"default" = CardholderStatusType::FRESH})
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CardholderStatusType")
     */
    private $status = CardholderStatusType::FRESH;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $creator;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Cardholder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname.
     *
     * @param string $surname
     *
     * @return Cardholder
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname.
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set middleName.
     *
     * @param string $middleName
     *
     * @return Cardholder
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName.
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $fullName = $this->surname;

        if ($this->name) {
            $fullName .= " {$this->name}";
        }

        if ($this->middleName) {
            $fullName .= " {$this->middleName}";
        }

        return $fullName;
    }

    /**
     * Set birthday.
     *
     * @param \DateTime|null $birthday
     *
     * @return Cardholder
     */
    public function setBirthday(\DateTime $birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return \DateTime|null
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set gender.
     *
     * @param string $gender
     *
     * @return Cardholder
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return Cardholder
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set car.
     *
     * @param string $car
     *
     * @return Cardholder
     */
    public function setCar($car)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car.
     *
     * @return string
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set carNumber.
     *
     * @param string $carNumber
     *
     * @return Cardholder
     */
    public function setCarNumber($carNumber)
    {
        $this->carNumber = $carNumber;

        return $this;
    }

    /**
     * Get carNumber.
     *
     * @return string
     */
    public function getCarNumber()
    {
        return $this->carNumber;
    }

    /**
     * Set carYear.
     *
     * @param int $carYear
     *
     * @return Cardholder
     */
    public function setCarYear($carYear)
    {
        $this->carYear = $carYear;

        return $this;
    }

    /**
     * Get carYear.
     *
     * @return int
     */
    public function getCarYear()
    {
        return $this->carYear;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return Cardholder
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set job.
     *
     * @param string|null $job
     *
     * @return Cardholder
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job.
     *
     * @return string|null
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set position.
     *
     * @param string|null $position
     *
     * @return Cardholder
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return Cardholder
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set number.
     *
     * @param string $number
     *
     * @return Cardholder
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param \DateTime|null $until
     *
     * @return Cardholder
     */
    public function setUntil($until)
    {
        $this->until = $until;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUntil()
    {
        return $this->until;
    }

    /**
     * @param string $status
     *
     * @return Cardholder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param User $creator
     *
     * @return Cardholder
     */
    public function setCreator(User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->getFullName();
        if ($result && $this->number) {
            $result .= sprintf(' [%s]', $this->number);
        }

        return $result ?: 'n/a';
    }
}
