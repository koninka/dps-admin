<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\Repository\PartnerRepository;

class PartnerManager extends BaseEntityManager
{
    /**
     * @return PartnerRepository
     */
    protected function getRepository()
    {
        return parent::getRepository();
    }
}
