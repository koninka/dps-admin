<?php

namespace AppBundle\Entity\Manager;

abstract class BaseEntityManager extends \Sonata\CoreBundle\Model\BaseEntityManager
{
    /**
     * @param object $entity
     *
     * @return object
     */
    public function merge($entity)
    {
        $this->checkObject($entity);

        return $this->getEntityManager()->merge($entity);
    }

    /**
     * @param string $class
     *
     * @return string mixed
     */
    protected function getTableNameByClass($class)
    {
        return $this->getObjectManager()->getClassMetadata($class)->table['name'];
    }
}
