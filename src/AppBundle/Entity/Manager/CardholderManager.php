<?php

namespace AppBundle\Entity\Manager;

use AppBundle\Entity\Repository\CardholderRepository;

class CardholderManager extends BaseEntityManager
{
    /**
     * @return CardholderRepository
     */
    protected function getRepository()
    {
        return parent::getRepository();
    }
}
