<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CardBrowsing.
 *
 * @ORM\Table(name="card_browsings")
 * @ORM\Entity()
 */
class CardBrowsing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Cardholder
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cardholder")
     * @ORM\JoinColumn(name="cardholder_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $cardholder;

    /**
     * @var Partner
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Partner")
     * @ORM\JoinColumn(name="partner_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $partner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="view_date", type="datetime")
     */
    private $viewDate;

    /**
     * CardBrowsing constructor.
     */
    public function __construct()
    {
        $this->viewDate = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardholder.
     *
     * @param Cardholder $cardholder
     *
     * @return CardBrowsing
     */
    public function setCardholder(Cardholder $cardholder)
    {
        $this->cardholder = $cardholder;

        return $this;
    }

    /**
     * Get cardholder.
     *
     * @return Cardholder
     */
    public function getCardholder()
    {
        return $this->cardholder;
    }

    /**
     * Set partner.
     *
     * @param Partner $partner
     *
     * @return CardBrowsing
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner.
     *
     * @return Partner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Set viewDate.
     *
     * @param \DateTime $viewDate
     *
     * @return CardBrowsing
     */
    public function setViewDate(\DateTime $viewDate)
    {
        $this->viewDate = $viewDate;

        return $this;
    }

    /**
     * Get viewDate.
     *
     * @return \DateTime
     */
    public function getViewDate()
    {
        return $this->viewDate;
    }
}
