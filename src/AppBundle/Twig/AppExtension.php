<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManagerInterface;

class AppExtension extends \Twig_Extension
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            'is_non_local_request' => new \Twig_Function_Method($this, 'isNonLocalRequest'),
        ];
    }

    /**
     * Is non local Request.
     *
     * @return bool
     */
    public function isNonLocalRequest()
    {
        return isset($_SERVER['HTTP_CLIENT_IP'])
        || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
        || !(in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', 'fe80::1', '::1']));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_extension';
    }
}
