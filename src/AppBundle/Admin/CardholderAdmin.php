<?php

namespace AppBundle\Admin;

use AppBundle\DBAL\Types\CardholderStatusType;
use AppBundle\DBAL\Types\GenderType;
use AppBundle\Entity\Cardholder;
use Application\Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CardholderAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->tabCreateLabel = 'menu.create_cardholder';
        $this->baseRouteName = 'admin_cardholders';
        $this->baseRoutePattern = 'cardholders';
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        /** @var Cardholder $object */
        $object = parent::getNewInstance();
        $object->setCreator($this->getUser());

        return $object;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('surname')
            ->add('middleName')
            ->add('gender', 'doctrine_orm_choice', [
                'field_options' => [
                    'choices' => GenderType::getChoices(),
                ],
                'field_type' => 'choice',
            ])
            ->add('phone')
            ->add('carNumber')
            ->add('number')
            ->add('status', 'doctrine_orm_choice', [
                'field_options' => [
                    'choices' => CardholderStatusType::getChoices(),
                ],
                'field_type' => 'choice',
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('fullName')
            ->add('phone')
            ->addIdentifier('number')
            ->add('car', null, [
                'editable' => true,
            ])
            ->add('until')
            ->add('status', 'choice', [
                'choices' => CardholderStatusType::getChoices(),
            ])
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('group.general', ['class' => 'col-md-6'])
                ->add('id', null, [
                    'required' => false,
                    'disabled' => true,
                ])
                ->add('name')
                ->add('surname')
                ->add('middleName')
                ->add('birthday', 'app_admin_sonata_datetime_picker')
                ->add('gender', 'choice', [
                    'choices' => GenderType::getChoices(),
                ])
                ->add('job')
                ->add('position')
                ->add('comment', null, [
                    'attr' => ['rows' => 5],
                ])
                ->add('createdAt', 'app_admin_sonata_datetime_picker', [
                    'disabled' => true,
                ])
                ->add('updatedAt', 'app_admin_sonata_datetime_picker', [
                    'disabled' => true,
                ])
            ->end()
            ->with('group.card_info', ['class' => 'col-md-6'])
                ->add('number', null, [
                    'help' => 'Укажите номер в следующем формате (6 символов): XXXXXX',
                    'attr' => [
                        'placeholder' => 'ABC123',
                    ],
                ])
                ->add('until', 'app_admin_sonata_datetime_picker')
                ->add('status', 'choice', [
                    'choices' => CardholderStatusType::getChoices(),
                ])
            ->end()
            ->with('group.contacts', ['class' => 'col-md-6'])
                ->add('phone', null, [
                    'help' => 'Укажите номер телефона в следующем формате: +79140760876',
                    'attr' => [
                        'placeholder' => '+79140760876',
                    ],
                ])
                ->add('address')
            ->end()
            ->with('group.car_info', ['class' => 'col-md-6'])
                ->add('car')
                ->add('carNumber')
                ->add('carYear')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('group.general', ['class' => 'col-md-6'])
                ->add('id')
                ->add('name')
                ->add('surname')
                ->add('middleName')
                ->add('birthday')
                ->add('gender', 'choice', [
                    'choices' => GenderType::getChoices(),
                ])
                ->add('job')
                ->add('position')
                ->add('comment')
                ->add('createdAt')
                ->add('updatedAt')
            ->end()
            ->with('group.card_info', ['class' => 'col-md-6'])
                ->add('number')
                ->add('until')
                ->add('status', 'choice', [
                    'choices' => CardholderStatusType::getChoices(),
                ])
            ->end()
            ->with('group.contacts', ['class' => 'col-md-6'])
                ->add('phone')
                ->add('address')
            ->end()
            ->with('group.car_info', ['class' => 'col-md-6'])
                ->add('car')
                ->add('carNumber')
                ->add('carYear')
            ->end()
        ;
    }
}
