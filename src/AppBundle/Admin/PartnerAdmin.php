<?php

namespace AppBundle\Admin;

use AppBundle\DBAL\Types\PartnerStatusType;
use AppBundle\Entity\Partner;
use Application\Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PartnerAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->tabCreateLabel = 'menu.create_partner';
        $this->baseRouteName = 'admin_partners';
        $this->baseRoutePattern = 'partners';
    }

    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        /** @var Partner $object */
        $object = parent::getNewInstance();
        $object->setCreator($this->getUser());

        return $object;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('organization')
            ->add('name')
            ->add('surname')
            ->add('middleName')
            ->add('phone')
            ->add('email')
            ->add('discount')
            ->add('status', 'doctrine_orm_choice', [
                'field_options' => [
                    'choices' => PartnerStatusType::getChoices(),
                ],
                'field_type' => 'choice',
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('organization', null, [
                'editable' => true,
            ])
            ->add('fullName')
            ->add('phone')
            ->add('email')
            ->add('discount')
            ->add('until')
            ->add('status', 'choice', [
                'choices' => PartnerStatusType::getChoices(),
            ])
            ->add('creator')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('group.general', ['class' => 'col-md-6'])
                ->add('id', null, [
                    'required' => false,
                    'disabled' => true,
                ])
                ->add('organization')
                ->add('status', 'choice', [
                    'choices' => PartnerStatusType::getChoices(),
                ])
                ->add('address', null, [
                    'attr' => [
                        'rows' => 4,
                    ],
                ])
                ->add('branch')
                ->add('discount', null, [
                    'attr' => [
                        'min' => 1,
                        'max' => 100,
                    ],
                ])
                ->add('until', 'app_admin_sonata_datetime_picker')
                ->add('createdAt', 'app_admin_sonata_datetime_picker', [
                    'disabled' => true,
                ])
                ->add('updatedAt', 'app_admin_sonata_datetime_picker', [
                    'disabled' => true,
                ])
            ->end()
            ->with('group.contacts', ['class' => 'col-md-6'])
                ->add('name')
                ->add('surname')
                ->add('middleName')
                ->add('phone', null, [
                    'help' => 'Укажите номер телефона в следующем формате: +79140760876',
                    'attr' => [
                        'placeholder' => '+79140760876',
                    ],
                ])
                ->add('email', 'email')
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('group.general', ['class' => 'col-md-6'])
                ->add('id')
                ->add('organization')
                ->add('status', 'choice', [
                    'choices' => PartnerStatusType::getChoices(),
                ])
                ->add('address')
                ->add('branch')
                ->add('discount')
                ->add('until')
                ->add('createdAt')
                ->add('updatedAt')
            ->end()
            ->with('group.contacts', ['class' => 'col-md-6'])
                ->add('name')
                ->add('surname')
                ->add('middleName')
                ->add('phone')
                ->add('email')
            ->end()
        ;
    }
}
