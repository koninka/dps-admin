<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class PartnerStatusType extends AbstractEnumType
{
    const FRESH      = 'fresh';
    const ACTIVE     = 'active';
    const NOT_ACTIVE = 'not_active';

    /**
     * @var array
     */
    protected static $choices = [
        self::FRESH      => 'Новый',
        self::ACTIVE     => 'Активный',
        self::NOT_ACTIVE => 'Неактивный',
    ];
}
