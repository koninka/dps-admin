<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class GenderType extends AbstractEnumType
{
    const MALE  = 'male';
    const FEMALE   = 'female';
    const UNKNOWN   = 'unknown';

    /**
     * @var array
     */
    protected static $choices = [
        self::MALE     => 'Мужской',
        self::FEMALE   => 'Женский',
        self::UNKNOWN  => 'Неизвестно',
    ];
}
