<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class CardholderStatusType extends AbstractEnumType
{
    const FRESH      = 'fresh';
    const ACTIVE     = 'active';
    const PRINTED    = 'printed';
    const PRINTING   = 'printing';
    const NOT_ACTIVE = 'not_active';

    /**
     * @var array
     */
    protected static $choices = [
        self::FRESH      => 'Новая',
        self::ACTIVE     => 'Активная',
        self::PRINTED    => 'Напечатанная',
        self::PRINTING   => 'В печати',
        self::NOT_ACTIVE => 'Неактивная',
    ];
}
