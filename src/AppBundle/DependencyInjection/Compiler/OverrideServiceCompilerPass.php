<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('sonata.user.orm.user_manager');
        $definition->setClass('Application\Sonata\UserBundle\Entity\Manager\UserManager');
//        $definition->addMethodCall('setTranslationDomain', [
//            'SonataUserBundle',
//        ]);
    }
}
