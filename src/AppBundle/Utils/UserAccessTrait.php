<?php

namespace AppBundle\Utils;

use Application\Sonata\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

trait UserAccessTrait
{
    /**
     * Get authorized user.
     *
     * @throws AccessDeniedException
     *
     * @return User
     */
    protected function getAuthUser()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw $this->createAccessDeniedException('This user does not have access to this section.');
        }

        return $user;
    }
}
