<?php

namespace AppBundle\Utils;

trait FlashTrait
{
    /**
     * Set flash.
     *
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }
}
