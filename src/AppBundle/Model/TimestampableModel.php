<?php

namespace AppBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 */
abstract class TimestampableModel implements TimestampableInterface
{
    use TimestampableTrait;
}
