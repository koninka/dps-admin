<?php

namespace AppBundle\Model;

interface PopulatableInterface
{
    /**
     * Populate current model by another.
     *
     * @param PopulatableInterface $object
     *
     * @return PopulatableInterface
     */
    public function populate(PopulatableInterface $object);
}
