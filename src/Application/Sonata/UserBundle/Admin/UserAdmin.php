<?php

namespace Application\Sonata\UserBundle\Admin;

use Application\Sonata\AdminBundle\Admin\AbstractAdmin;
use Application\Sonata\UserBundle\Entity\Manager\UserManager;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->tabCreateLabel = 'menu.create_user';
        $this->baseRouteName = 'admin_users';
        $this->baseRoutePattern = 'users';
    }

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] = (!$this->getSubject() || is_null($this->getSubject()->getId())) ? 'Registration' : 'Profile';

        $formBuilder = $this->getFormContractor()->getFormBuilder($this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        // avoid security field to be exported
        return array_filter(parent::getExportFields(), function ($v) {
            return !in_array($v, ['password', 'salt'], true);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    /**
     * @param UserManager $userManager
     */
    public function setUserManager(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManager
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General', ['class' => 'col-md-6'])
                    ->add('username')
                    ->add('email')
                    ->add('plainPassword', 'text', [
                        'required' => (!$this->getSubject() || is_null($this->getSubject()->getId())),
                    ])
                    ->add('configuredEmail', null, ['required' => false])
                    ->add('comment', 'textarea', [
                        'required' => false,
                        'attr' => ['rows' => 5],
                    ])
                ->end()
                ->with('Profile', ['class' => 'col-md-6'])
                    ->add('firstname', null, ['required' => false])
                    ->add('lastname', null, ['required' => false])
                ->end()
            ->end()
        ;

        $isExistManagement = $this->getSubject() && !$this->getSubject()->hasRole('ROLE_SUPER_ADMIN');
        $formMapper
            ->tab('Security')
                ->with('Groups', ['class' => sprintf('col-md-%d', $isExistManagement ? 6 : 12)])
                    ->add('groups', 'sonata_type_model', [
                        'required' => false,
                        'expanded' => true,
                        'multiple' => true,
                    ])
                ->end()
            ->end()
        ;

        if ($isExistManagement) {
            $formMapper
                ->tab('Security')
                    ->with('Management', ['class' => 'col-md-6'])
                        ->add('realRoles', 'sonata_security_roles', [
                            'label'    => 'form.label_roles',
                            'expanded' => true,
                            'multiple' => true,
                            'required' => false,
                        ])
                        ->add('locked', null, ['required' => false])
                        ->add('expired', null, ['required' => false])
                        ->add('enabled', null, ['required' => false])
                        ->add('credentialsExpired', null, ['required' => false])
                    ->end()
                ->end()
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
                ->add('id')
                ->add('username')
                ->add('email')
                ->add('configuredEmail')
                ->add('comment')
            ->end()
            ->with('Groups')
                ->add('groups')
            ->end()
            ->with('Profile')
                ->add('firstname')
                ->add('lastname')
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('username')
            ->addIdentifier('email')
            ->add('enabled', null, ['editable' => true])
            ->add('locked', null, ['editable' => true])
            ->add('createdAt')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add('username')
            ->add('locked')
            ->add('email')
            ->add('configuredEmail')
            ->add('groups')
        ;
    }
}
