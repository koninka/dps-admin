<?php

namespace Application\Sonata\UserBundle\Menu;

use Application\Sonata\UserBundle\Entity\User;
use D2P\MainBundle\Helper\Arr;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;

class UserMenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * Constructor.
     *
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Create main user menu (top).
     *
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMainMenu(array $options = [])
    {
        $menu = $this->factory->createItem('root');

        if (!array_key_exists('user', $options)) {
            return $menu;
        }

        /** @var User $user */
        $user = $options['user'];

        $menu->addChild(
            'link_profile_settings',
            [
                'icon' => 'cog',
                'route' => 'fos_user_profile_edit',
                'extras' => ['translation_domain' => 'SonataUserBundle'],
            ]
        );

        $menu->addChild('sonata_profile_title', [
            'icon' => 'user',
            'route' => 'fos_user_profile_show',
            'extras' => ['translation_domain' => 'SonataUserBundle'],
        ]);

//        $currentName = Arr::get($options, 'current');
//        if ($currentName !== null && isset($menu[$currentName])) {
//            $menu[$currentName]->setCurrent(true);
//        }

        return $menu;
    }

    /**
     * Create settings menu.
     *
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createSettingsMenu(array $options = [])
    {
        $menu = $this->factory->createItem('root');

        if (!array_key_exists('user', $options)) {
            return $menu;
        }

        /** @var User $user */
        $user = $options['user'];

        $menu->addChild('link_edit_profile', [
            'icon' => 'pencil',
            'route' => 'fos_user_profile_edit',
            'extras' => ['translation_domain' => 'SonataUserBundle'],
        ]);
        $menu->addChild('title_user_edit_authentication', [
            'icon' => 'envelope',
            'route' => 'fos_user_profile_edit_authentication',
            'extras' => ['translation_domain' => 'SonataUserBundle'],
        ]);

        if ($user->isConfiguredEmail()) {
            $menu->addChild('sonata_change_password_link', [
                'icon' => 'key',
                'route' => 'fos_user_change_password',
                'extras' => ['translation_domain' => 'SonataUserBundle'],
            ]);
        }

//        $menu->addChild('title_social_accounts', [
//            'icon' => 'lock',
//            'route' => 'app_sonata_user_edit_accounts',
//            'extras' => ['translation_domain' => 'SonataUserBundle'],
//        ]);

        return $menu;
    }
}
