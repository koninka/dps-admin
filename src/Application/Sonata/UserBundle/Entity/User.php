<?php

namespace Application\Sonata\UserBundle\Entity;

use Application\Sonata\UserBundle\Model\User as AbstractedUser;

class User extends AbstractedUser
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var bool
     */
    protected $configuredEmail = true;

    /**
     * @var string
     */
    protected $comment;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set configured email.
     *
     * @param bool $configuredEmail
     *
     * @return User
     */
    public function setConfiguredEmail($configuredEmail)
    {
        $this->configuredEmail = $configuredEmail;

        return $this;
    }

    /**
     * Is configured email.
     *
     * @return bool
     */
    public function isConfiguredEmail()
    {
        return $this->configuredEmail;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return User
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets the email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        if (empty($this->getUsername())) {
            $this->setUsername($email);
        }

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     *
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        if (empty($this->getUsername())) {
            $this->setUsernameCanonical($emailCanonical);
        }

        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * Get user display name.
     *
     * @return string
     */
    public function getDisplayName()
    {
        $name = trim($this->getFullname());

        return $name !== '' ? $name : $this->email;
    }

    /**
     * Hook on pre-persist operations.
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Hook on pre-update operations.
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
