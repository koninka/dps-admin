<?php

namespace Application\Sonata\UserBundle\Entity\Manager;

use Application\Sonata\UserBundle\Entity\User;
use Sonata\UserBundle\Entity\UserManager as BaseUserManager;

class UserManager extends BaseUserManager
{
    /**
     * Find user by given username or email.
     *
     * @param string $username
     * @param string $email
     *
     * @return User|null
     */
    public function findUserByUsernameAndEmail($username, $email)
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.username = :username')
            ->orWhere('u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
