<?php

namespace Application\Sonata\UserBundle\Form\Handler;

use Application\Sonata\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class ProfileAuthFormHandler
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var Form
     */
    protected $form;

    /**
     * Constructor.
     *
     * @param Request              $request
     * @param UserManagerInterface $userManager
     */
    public function __construct(Request $request, UserManagerInterface $userManager)
    {
        $this->request = $request;
        $this->userManager = $userManager;
    }

    /**
     * @param Form $form
     * @param User $user
     *
     * @return bool
     */
    public function process(Form $form, User $user)
    {
        $this->form = $form;

        if ('POST' === $this->request->getMethod()) {
            $this->form->handleRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($user);

                return true;
            }

            // Reloads the user to reset its username. This is needed when the
            // username or password have been changed to avoid issues with the
            // security layer.
            $this->userManager->reloadUser($user);
        }

        return false;
    }

    /**
     * On success submit.
     *
     * @param User $user
     */
    protected function onSuccess(User $user)
    {
        $user->setConfiguredEmail(true);

        $this->userManager->updateUser($user);
    }
}
