<?php

namespace Application\Sonata\UserBundle\Form\Handler;

use Application\Sonata\UserBundle\Entity\User;
use FOS\UserBundle\Form\Model\ChangePassword;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class ChangePasswordFormHandler
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var Form
     */
    protected $form;

    public function __construct(Request $request, UserManagerInterface $userManager)
    {
        $this->request = $request;
        $this->userManager = $userManager;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->form->getData()->new;
    }

    /**
     * Process form.
     *
     * @param Form $form
     * @param User $user
     *
     * @return bool
     */
    public function process(Form $form, User $user)
    {
        $this->form = $form;
        $this->form->setData(new ChangePassword());

        if ('POST' === $this->request->getMethod()) {
            $this->form->handleRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($user);

                return true;
            }
        }

        return false;
    }

    /**
     * On success handler.
     *
     * @param User $user
     */
    protected function onSuccess(User $user)
    {
        $user->setPlainPassword($this->getNewPassword());
        $user->setConfiguredPassword(true);
        $this->userManager->updateUser($user);
    }
}
