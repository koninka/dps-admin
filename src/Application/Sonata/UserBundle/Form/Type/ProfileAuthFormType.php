<?php

namespace Application\Sonata\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ProfileAuthFormType extends AbstractType
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var array
     */
    private $validationGroups;

    /**
     * Constructor.
     *
     * @param string $class            The User class name
     * @param array  $validationGroups
     */
    public function __construct($class, array $validationGroups)
    {
        $this->class = $class;
        $this->validationGroups = $validationGroups;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $constraint = new UserPassword();

        $builder->add('email', 'email', [
            'data' => $options['email'],
            'label' => 'form.email',
            'required' => true,
            'translation_domain' => 'FOSUserBundle',
        ]);

        if ($options['has_pass']) { # email not configured
            $builder->add('current_password', 'password', [
                'label' => 'form.current_password',
                'translation_domain' => 'FOSUserBundle',
                'mapped' => false,
                'constraints' => $constraint,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'email'             => null,
            'has_pass'          => true,
            'data_class'        => $this->class,
            'intention'         => 'profile',
            'validation_groups' => $this->validationGroups,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_user_profile_auth';
    }
}
