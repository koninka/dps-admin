<?php

namespace Application\Sonata\UserBundle\Form\Type;

use Sonata\UserBundle\Form\Type\ProfileType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileType extends BaseType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, [
                'label'    => 'form.label_firstname',
                'required' => false,
                'translation_domain' => 'SonataUserBundle',
            ])
            ->add('lastname', null, [
                'label'    => 'form.label_lastname',
                'required' => false,
            ])
            ->add('website', 'url', [
                'label'    => 'form.label_website',
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_user_profile';
    }
}
