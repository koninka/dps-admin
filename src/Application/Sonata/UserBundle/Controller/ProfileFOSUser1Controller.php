<?php

namespace Application\Sonata\UserBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use Sonata\UserBundle\Controller\ProfileFOSUser1Controller as BaseProfileFOSUser1Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VenVit\AppBundle\Utils\UserAccessTrait;

class ProfileFOSUser1Controller extends BaseProfileFOSUser1Controller
{
    use UserAccessTrait;

    /**
     * @throws AccessDeniedException
     *
     * @return Response
     */
    public function editProfileAction()
    {
        $user = $this->getAuthUser();

        $form = $this->container->get('sonata.user.profile.form');
        $formHandler = $this->container->get('sonata.user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('sonata_user_success', 'profile.flash.updated');

            return $this->redirectToRoute('fos_user_profile_edit');
        }

//        $title = 'Профиль - редактирование основных данных';
//        $this->addMeta(null, $title, $title);

        return $this->render('SonataUserBundle:Profile:edit_profile.html.twig', [
            'form'               => $form->createView(),
            'breadcrumb_context' => 'user_profile',
        ]);
    }

    /**
     * @throws AccessDeniedException
     *
     * @return Response
     */
    public function editAuthenticationAction()
    {
        $user = $this->getAuthUser();

        $form = $this->createForm('app_user_profile_auth', $user, [
            'email'    => $user->isConfiguredEmail() ? $user->getEmail() : null,
            'has_pass' => $user->isConfiguredPassword(),
        ]);
        $formHandler = $this->get('sonata.user.authentication.form_handler');

        $process = $formHandler->process($form, $user);
        if ($process) {
            $this->setFlash('sonata_user_success', 'change_auth.flash.success');

            return $this->redirectToRoute('fos_user_profile_edit_authentication');
        }

//        $title = 'Профиль - изменение e-mail';
//        $this->addMeta(null, $title, $title);

        return $this->render('SonataUserBundle:Profile:edit_authentication.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
