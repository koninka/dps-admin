<?php

namespace Application\Sonata\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityFOSUser1Controller extends SecurityController
{
    /**
     * {@inheritdoc}
     */
    public function loginAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();

        if ($user instanceof UserInterface) {
            $this->container->get('session')->getFlashBag()->set('sonata_user_error', 'sonata_user_already_authenticated');
            $url = $this->container->get('router')->generate('sonata_user_profile_show');

            return new RedirectResponse($url);
        }

//        $this->addPageMeta(7, 'Авторизация', '');

        return parent::loginAction();
    }

    /**
     * {@inheritdoc}
     */
    protected function renderLogin(array $data)
    {
        $request = $this->container->get('request');
        $target = $request->query->get('target');
        if ($target !== null) {
            $request->getSession()->set('_security_target_path', $target);
        } else {
            $target = $request->getSession()->get('_security_target_path');
        }

        $data['last_target_path'] = $target;

        $template = sprintf('FOSUserBundle:Security:login.html.%s', $this->container->getParameter('fos_user.template.engine'));

        return $this->container->get('templating')->renderResponse($template, $data);
    }
}
