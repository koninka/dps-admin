<?php

namespace Application\Sonata\UserBundle\Controller;

use Sonata\UserBundle\Controller\ResettingFOSUser1Controller as BaseResettingFOSUser1Controller;

class ResettingFOSUser1Controller extends BaseResettingFOSUser1Controller
{
    /**
     * {@inheritdoc}
     */
    public function requestAction()
    {
        //        $this->addPageMeta(8, 'Восстановление пароля', '');

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:request.html.' . $this->getEngine());
    }
}
