<?php

namespace Application\Sonata\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use VenVit\AppBundle\Utils\UserAccessTrait;

class ChangePasswordFOSUser1Controller extends Controller
{
    use UserAccessTrait;

    /**
     * @throws AccessDeniedException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction()
    {
        $user = $this->getAuthUser();

        $form = $this->createForm('app_user_change_password', null, ['has_pass' => $user->isConfiguredPassword()]);
        $formHandler = $this->container->get('fos_user.change_password.form.handler');

        $process = $formHandler->process($form, $user);
        if ($process) {
            $this->setFlash('sonata_user_success', 'change_password.flash.success');

            return $this->redirectToRoute('fos_user_change_password');
        }

//        $title = 'Профиль - изменение пароля';
//        $this->addMeta(null, $title, $title);

        return $this->render(
            'SonataUserBundle:ChangePassword:changePassword.html.' . $this->container->getParameter('fos_user.template.engine'),
            ['form' => $form->createView()]
        );
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }
}
