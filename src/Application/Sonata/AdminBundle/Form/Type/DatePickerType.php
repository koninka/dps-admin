<?php

namespace Application\Sonata\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatePickerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'dp_language' => 'en',
            'view_timezone' => 'Israel',
            'model_timezone' => 'Israel',
            'format' => 'dd.MM.yyyy',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'sonata_type_date_picker';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_admin_sonata_date_picker';
    }
}
