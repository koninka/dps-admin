<?php

namespace Application\Sonata\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateTimePickerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'dp_language' => 'ru',
            'view_timezone' => 'Asia/Vladivostok',
            'model_timezone' => 'Asia/Vladivostok',
            'format' => 'dd.MM.yyyy, HH:mm',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'sonata_type_datetime_picker';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_admin_sonata_datetime_picker';
    }
}
