<?php

namespace Application\Sonata\AdminBundle\Admin;

use AppBundle\Model\PopulatableInterface;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin as BaseAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;

class AbstractAdmin extends BaseAdmin
{
    const LINK_ACTION_CREATE = 'link_action_create';

    /**
     * @var array
     */
    protected $linkActions = [
        'link_action_create' => ['id' => 'link_action_create', 'domain' => 'SonataAdminBundle'],
    ];

    /**
     * @var string|null
     */
    protected $tabCreateLabel;

    /**
     * @param string $code
     *
     * @return string
     */
    public function transLinkAction($code)
    {
        $id = $code;
        $domain = 'SonataAdminBundle';
        if (array_key_exists($code, $this->linkActions)) {
            $id = $this->linkActions[$code]['id'];
            $domain = $this->linkActions[$code]['domain'];
        }

        return $this->trans($id, [], $domain);
    }

    /**
     * {@inheritdoc}
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        unset($list['create']);

        if ($object instanceof PopulatableInterface && in_array($action, ['show', 'edit'], true) && $this->hasAccess('create') && $this->hasRoute('create')) {
            $list['duplicate'] = [
                'template' => 'ApplicationSonataAdminBundle:Button:duplicate_button.html.twig',
            ];
        }

        return $list;
    }

    /**
     * @return ModelManager
     */
    public function getModelManager()
    {
        return parent::getModelManager();
    }

    /**
     * @param string $code
     * @param string $name
     * @param string $translationDomain
     */
    protected function setLinkAction($code, $name = null, $translationDomain = 'SonataAdminBundle')
    {
        $name = $name ?: $code;
        $this->linkActions[$code] = [
            'id'     => $name,
            'domain' => $translationDomain,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function configureBatchActions($actions)
    {
        unset($actions['delete']);

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ($this->tabCreateLabel === null || $action === 'create' || !$this->hasAccess('create')) {
            return;
        }

        $menu
            ->addChild($this->tabCreateLabel, [
                'uri' => $this->generateUrl('create'),
            ])
            ->setAttribute('icon', 'fa fa-plus-circle')
            ->setExtra('translation_domain', $this->getTranslationDomain())
        ;
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @return null|\Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function getContainer()
    {
        return $this->getConfigurationPool()->getContainer();
    }

    /**
     * @return \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     */
    protected function getAuthChecker()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager()
    {
        return $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this->getEntityManager()->getRepository($this->getClass());
    }
}
