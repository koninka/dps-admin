<?php

namespace Application\Sonata\AdminBundle\Controller;

use AppBundle\Model\PopulatableInterface;
use AppBundle\Role\RoleManager;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CRUDController extends Controller
{
    /**
     * Delete action.
     *
     * @param int|string|null $id
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response|RedirectResponse
     */
    public function deleteAction($id)
    {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $this->admin->checkAccess('delete', $object);

        $preResponse = $this->preDelete($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($this->getRestMethod() == 'DELETE') {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            $objectName = $this->admin->toString($object);

            try {
                $this->admin->delete($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(['result' => 'ok'], 200, []);
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->getDeleteSuccessMessage($object, $objectName)
                );
            } catch (ModelManagerException $e) {
                $this->handleModelManagerException($e);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(['result' => 'error'], 200, []);
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans(
                        'flash_delete_error',
                        ['%name%' => $this->escapeHtml($objectName)],
                        'SonataAdminBundle'
                    )
                );
            }

            return $this->redirectTo($object);
        }

        return $this->render($this->admin->getTemplate('delete'), [
            'object' => $object,
            'action' => 'delete',
            'csrf_token' => $this->getCsrfToken('sonata.delete'),
        ], null);
    }

    /**
     * {@inheritdoc}
     */
    protected function preCreate(Request $request, $object)
    {
        if (!$object instanceof PopulatableInterface) {
            return;
        }

        if (($cloneId = $request->get('oid')) === null) {
            return;
        }
        /** @var PopulatableInterface|null $sourceObject */
        $sourceObject = $this->admin->getObject($cloneId);
        if ($sourceObject === null) {
            return;
        }

        $object->populate($sourceObject);
    }

    /**
     * @param bool $hasFilter
     *
     * @return RedirectResponse
     */
    protected function redirectToList($hasFilter = true)
    {
        $params = $hasFilter ? ['filter' => $this->admin->getFilterParameters()] : [];

        return new RedirectResponse(
            $this->admin->generateUrl(
                'list',
                $params
            )
        );
    }

    /**
     * @param mixed $object
     * @param string
     *
     * @return string
     */
    protected function getDeleteSuccessMessage($object, $objectName)
    {
        return $this->admin->trans(
            'flash_delete_success',
            ['%name%' => $this->escapeHtml($objectName)],
            'SonataAdminBundle'
        );
    }

    /**
     * @return bool
     */
    protected function isSuperAdmin()
    {
        return $this->isGranted(RoleManager::ROLE_SUPER_ADMIN);
    }
}
