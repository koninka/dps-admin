THIS_FILE := $(lastword $(MAKEFILE_LIST))

cs:
	php-cs-fixer fix --verbose

cs-dry-run:
	php-cs-fixer fix --verbose --dry-run

c-inst:
	composer install

test:
	bin/phpunit -c app src --process-isolation

test-failing:
	bin/phpunit -c app src --group failing --process-isolation

dev: set-permissions cache-dev install-assets
	$(MAKE) -f $(THIS_FILE) set-permissions

prod: set-permissions cache-prod dump install-assets
	$(MAKE) -f $(THIS_FILE) set-permissions

set-permissions:
	sudo chmod -R ug+rw .
	sudo chmod -R a+rw app/cache app/logs app/spool

clear-cache-dev: set-permissions cache-dev
	$(MAKE) -f $(THIS_FILE) set-permissions

clear-cache-prod: set-permissions cache-prod
	$(MAKE) -f $(THIS_FILE) set-permissions

cache-dev:
	php5 app/console cache:clear --env=dev

cache-prod:
	php5 app/console cache:clear --env=prod --no-debug

update-db:
	php5 app/console doctrine:schema:update --force --dump-sql

reload-db:
	php5 app/console doctrine:database:drop --force
	$(MAKE) -f $(THIS_FILE) create-db

create-db:
	php5 app/console doctrine:database:create

create-schema:
	php5 app/console doctrine:schema:create

reload-db-test:
	php5 app/console doctrine:database:drop --force --env=test
	php5 app/console doctrine:database:create --env=test
	php5 app/console doctrine:schema:create --env=test

load-fixtures:
	php5 app/console doctrine:fixtures:load

load-hautelook-fixtures:
	php5 app/console hautelook_alice:doctrine:fixtures:load

migrate:
	php5 app/console doctrine:migrations:migrate latest

dump:
	composer dump-autoload --optimize
	php5 app/console assetic:dump --env=prod --no-debug

install-assets: install-web-assets

install-web-assets:
	php5 app/console assets:install web --symlink
