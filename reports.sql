-- Отчеты директора

-- Отчет по всем продажам за указанный период - кто продал и сколько
SELECT
    u.id,
    u.email,
    u.firstname,
    u.lastname,
    COUNT(c.id) as amount
FROM users__users u
LEFT JOIN cardholders c ON c.creator_id = u.id AND c.created_at BETWEEN '2016-12-24 00:00:00' AND '2016-12-25 23:59:59'
GROUP BY u.id

-- Отчет по всем партнерам за указанный период - кто подключил и сколько
SELECT
    u.id,
    u.email,
    u.firstname,
    u.lastname,
    COUNT(p.id) as amount
FROM users__users u
LEFT JOIN partners p ON p.creator_id = u.id AND p.created_at BETWEEN '2016-10-24 00:00:00' AND '2016-12-23 23:59:59'
GROUP BY u.id

-- Отчет о посещениях за указзный период - кто именно посетил (держатель) и кого (партнер) и когда
-- TO DO


-- Печатная организация
-- документ, в котором содержится, какие карты нужно распечатать (фамилия, имя, номер карты)
SELECT
    c.id,
    c.name,
    c.surname,
    c.middleName
FROM cardholders c
WHERE c.status = 'printing';
